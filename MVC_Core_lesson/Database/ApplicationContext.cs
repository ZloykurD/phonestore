﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MVC_Core_lesson.Models;
using Newtonsoft.Json;

namespace MVC_Core_lesson.Database
{
  public class ApplicationContext : DbContext
  {
    public DbSet<Phone> Phones { get; set; }
    public DbSet<Order> Orders { get; set; }
    public DbSet<Category> Categories { get; set; }
    public DbSet<Stock> Stocks { get; set; }
    public DbSet<PhoneOnStock> PhoneOnStocks { get; set; }
    public DbSet<Money> Monies { get; set; }
    public DbSet<Company> Companies { get; set; }
    public DbSet<Description> Descriptions { get; set; }


    public ApplicationContext()
    {
    }

    public ApplicationContext(DbContextOptions options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      base.OnModelCreating(modelBuilder);
      modelBuilder.Entity<Order>()
        .HasOne(o => o.Phone)
        .WithMany(o => o.Orders)
        .HasForeignKey(o => o.PhoneId);

      modelBuilder.Entity<Phone>()
        .HasMany(o => o.Orders)
        .WithOne(o => o.Phone)
        .HasPrincipalKey(o => o.Id)
        .OnDelete(DeleteBehavior.Restrict);


      modelBuilder.Entity<Phone>()
        .HasOne(p => p.Category)
        .WithMany(p => p.Phones)
        .HasForeignKey(p => p.CategoryId);

      modelBuilder.Entity<Category>()
        .HasMany(p => p.Phones)
        .WithOne(p => p.Category)
        .HasPrincipalKey(p => p.Id)
        .OnDelete(DeleteBehavior.Restrict);
      //рекурсия
      modelBuilder.Entity<Category>()
        .HasMany(c => c.SubCategories)
        .WithOne(c => c.ParentCategory)
        .HasForeignKey(c => c.ParentCategoryId);

      modelBuilder.Entity<Category>()
        .HasOne(c => c.ParentCategory)
        .WithMany(c => c.SubCategories)
        .HasPrincipalKey(c => c.Id)
        .OnDelete(DeleteBehavior.Restrict);

      //многие ко многим
      modelBuilder.Entity<PhoneOnStock>()
        .HasKey(c => new {c.PhoneId, c.StockId});

      modelBuilder.Entity<PhoneOnStock>()
        .HasOne(c => c.Phone)
        .WithMany(c => c.Stocks)
        .HasForeignKey(c => c.PhoneId);

      modelBuilder.Entity<PhoneOnStock>()
        .HasOne(c => c.Stock)
        .WithMany(c => c.Phones)
        .HasForeignKey(c => c.StockId);


      modelBuilder.Entity<Stock>()
        .HasData(JsonConvert.DeserializeObject<Stock[]>(File.ReadAllText("Seed/Stocks.json")));

      modelBuilder.Entity<Category>()
        .HasData(JsonConvert.DeserializeObject<Category[]>(File.ReadAllText("Seed/Categories.json")));

      modelBuilder.Entity<Phone>()
        .HasData(JsonConvert.DeserializeObject<Phone[]>(File.ReadAllText("Seed/Phones.json")));

      modelBuilder.Entity<Money>()
        .HasData(JsonConvert.DeserializeObject<Money[]>(File.ReadAllText("Seed/Money.json")));

      modelBuilder.Entity<Company>()
        .HasData(JsonConvert.DeserializeObject<Company[]>(File.ReadAllText("Seed/Companies.json")));

      modelBuilder.Entity<Phone>()
        .HasOne(p => p.Money)
        .WithMany(p => p.Phones)
        .HasForeignKey(p => p.MoneyId);

      modelBuilder.Entity<Money>()
        .HasMany(p => p.Phones)
        .WithOne(p => p.Money)
        .HasPrincipalKey(p => p.Id)
        .OnDelete(DeleteBehavior.Restrict);

      modelBuilder.Entity<Phone>()
        .HasOne(p => p.Company)
        .WithMany(p => p.Phones)
        .HasForeignKey(p => p.CompanyId)
        .OnDelete(DeleteBehavior.Restrict);

      modelBuilder.Entity<Company>()
        .HasMany(c => c.Phones)
        .WithOne(c => c.Company)
        .HasPrincipalKey(c => c.Id)
        .OnDelete(DeleteBehavior.Restrict);
        
      modelBuilder.Entity<Description>()
        .HasOne(o => o.Phone)
        .WithMany(o => o.Descriptions)
        .HasForeignKey(o => o.PhoneId);

      modelBuilder.Entity<Phone>()
        .HasMany(o => o.Descriptions)
        .WithOne(o => o.Phone)
        .HasPrincipalKey(o => o.Id)
        .OnDelete(DeleteBehavior.Restrict);



    }
  }
}