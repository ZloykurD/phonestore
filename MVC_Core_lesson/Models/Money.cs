﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using MVC_Core_lesson.Secvices;

namespace MVC_Core_lesson.Models
{
  public class Money : Entity
  {
    [Display(Name = "Код валюты")]
    public String CurrencyCode { get; set; }
    [Display(Name = "Валюта")]
    public String CurrencyName { get; set; }
    [Display(Name = "Значение")]
    public double CurrencyRate { get; set; }

    public IEnumerable<Phone> Phones { get; set; }
  }
}
