﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MVC_Core_lesson.Secvices;

namespace MVC_Core_lesson.Models
{
  public class Phone : Entity
  {

    [Required(ErrorMessage = "Укажите модель")]
    [Display(Name = "Модель")]
    [Remote(action: "ValidateName",controller: "Validation",ErrorMessage = "Пользователь уже есть ")]
    public string Name { get; set; }

    [Required]
    [Display(Name = "Производитель")] 
    public int CompanyId { get; set; }
    

    [Display(Name = "Производитель")] public Company Company { get; set; }
    [Display(Name = "Цена")] public double Price { get; set; }
    [Display(Name = "Категория")] public int CategoryId { get; set; }
    [Display(Name = "Категория")] public Category Category { get; set; }
    [Display(Name = "Валюта")] public int MoneyId { get; set; }
    [Display(Name = "Валюта")] public Money Money { get; set; }
    [Display(Name = "На складе")] public bool InStock { get; set; }

    [Display(Name = "год выпуска")] public DateTime Date { get; set; }


    public IEnumerable<PhoneOnStock> Stocks { get; set; }
    public IEnumerable<Order> Orders { get; set; }
    public IEnumerable<Money> Monies { get; set; }
    public IEnumerable<Company> Companies { get; set; }
    public IEnumerable<Description> Descriptions { get; set; }
  }
}