﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_Core_lesson.Models
{
  public class PhoneOnStock
  {
  
    public int PhoneId{ get; set; }
    public Phone Phone { get; set; }

    public int  StockId { get; set; }
    public Stock Stock { get; set; }

    public int  Qty { get; set; }

  }
}
