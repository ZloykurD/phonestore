﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_Core_lesson.Models
{
  public class SearchResult
  {
    public String Name { get; set; }
    public double MinPrice { get; set; }
    public double MaxPrice { get; set; }
    public bool isCheck { get; set; }
  }
}