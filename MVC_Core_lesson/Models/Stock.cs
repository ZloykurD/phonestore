﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MVC_Core_lesson.Secvices;

namespace MVC_Core_lesson.Models
{
  public class Stock : Entity
  {
    public string Name { get; set; }

    public IEnumerable<PhoneOnStock> Phones { get; set; }
  }
}
