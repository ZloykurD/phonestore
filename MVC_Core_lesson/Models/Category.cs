﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using MVC_Core_lesson.Secvices;

namespace MVC_Core_lesson.Models
{
  public class Category: Entity
  {
    [Display(Name = "Категория")]
    public string Name { get; set; }

    public int? ParentCategoryId { get; set; }
    public Category ParentCategory { get; set; }

    public IEnumerable<Category> SubCategories { get; set; }
    public IEnumerable<Phone> Phones { get; set; }
  }
}