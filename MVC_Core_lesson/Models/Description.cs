﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using MVC_Core_lesson.Secvices;

namespace MVC_Core_lesson.Models
{
  public class Description : Entity
  {
    [Required(ErrorMessage = "Укажите имя")]
    [Display(Name = "Имя")]
    public String Name { get; set; }
    [Required(ErrorMessage = "Укажите описание")]
    [Display(Name = "Описание")]
    public String Text { get; set; }
    [Required(ErrorMessage = "Дайте оценку телефону")]
    [Display(Name = "Оценка")]
    public int Rating { get; set; }
    [Display(Name = "Телефон")]
    public int PhoneId { get; set; }
    [Display(Name = "Телефон")]
    public Phone Phone { get; set; }


    IEnumerable<Phone> Phones { get; set; }

  }
}