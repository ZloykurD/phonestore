﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MVC_Core_lesson.Secvices;

namespace MVC_Core_lesson.Models
{
  public class Company : Entity
  {

    [Required]
    [Display(Name = "Компания")]
    [Remote(action: "ValidateCompanyName", controller: "Validation", ErrorMessage = "Компания уже есть")]
    public String Name { get; set; }

    [Required]
    [EmailAddress(ErrorMessage = "Не верный формат")]
    [Display(Name = "Email")]
    public String Email { get; set; }

    [Required]
    [Display(Name = "Дата основания")]
    public DateTime Date { get; set; }

    public IEnumerable<Phone> Phones { get; set; }
  }
}
