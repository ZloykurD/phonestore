﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MVC_Core_lesson.Secvices;

namespace MVC_Core_lesson.Models
{
  public class Order : Entity
  {
    public string User { get; set; }
    public string Address { get; set; }
    public string ContactPhone { get; set; }

    public int PhoneId { get; set; }
    public Phone Phone { get; set; }
  }
}