﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_Core_lesson.Secvices
{
  public interface IValidator<in T> where T : Entity
  {
     List<ErrorMessage> Validate(T entity);
  }


}
