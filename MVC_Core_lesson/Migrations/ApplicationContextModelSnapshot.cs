﻿// <auto-generated />
using System;
using MVC_Core_lesson.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace MVC_Core_lesson.Migrations
{
    [DbContext(typeof(ApplicationContext))]
    partial class ApplicationContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.2-rtm-30932")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("MVC_Core_lesson.Models.Category", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name");

                    b.Property<int?>("ParentCategoryId");

                    b.HasKey("Id");

                    b.HasIndex("ParentCategoryId");

                    b.ToTable("Categories");

                    b.HasData(
                        new { Id = 1, Name = "IOS", ParentCategoryId = 1 },
                        new { Id = 2, Name = "Android", ParentCategoryId = 1 }
                    );
                });

            modelBuilder.Entity("MVC_Core_lesson.Models.Company", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("Date");

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int?>("PhoneId");

                    b.HasKey("Id");

                    b.HasIndex("PhoneId");

                    b.ToTable("Companies");

                    b.HasData(
                        new { Id = 1, Date = new DateTime(1976, 4, 1, 12, 50, 0, 0, DateTimeKind.Unspecified), Email = "hr@apple.com", Name = "Apple" },
                        new { Id = 2, Date = new DateTime(1938, 3, 1, 12, 50, 0, 0, DateTimeKind.Unspecified), Email = "hr@samsung.com", Name = "Samsung" },
                        new { Id = 3, Date = new DateTime(1865, 5, 12, 12, 50, 0, 0, DateTimeKind.Unspecified), Email = "hr@Nokia.com", Name = "Nokia" },
                        new { Id = 4, Date = new DateTime(2010, 4, 6, 12, 50, 0, 0, DateTimeKind.Unspecified), Email = "hr@Xiaomi.com", Name = "Xiaomi" },
                        new { Id = 5, Date = new DateTime(1997, 5, 15, 12, 50, 0, 0, DateTimeKind.Unspecified), Email = "hr@HTC.com", Name = "HTC" }
                    );
                });

            modelBuilder.Entity("MVC_Core_lesson.Models.Description", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int>("PhoneId");

                    b.Property<int>("Rating");

                    b.Property<string>("Text")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("PhoneId");

                    b.ToTable("Descriptions");
                });

            modelBuilder.Entity("MVC_Core_lesson.Models.Money", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("CurrencyCode");

                    b.Property<string>("CurrencyName");

                    b.Property<double>("CurrencyRate");

                    b.Property<int?>("PhoneId");

                    b.HasKey("Id");

                    b.HasIndex("PhoneId");

                    b.ToTable("Monies");

                    b.HasData(
                        new { Id = 1, CurrencyCode = "RUB", CurrencyName = "�����", CurrencyRate = 57.0 },
                        new { Id = 2, CurrencyCode = "KGS", CurrencyName = "���", CurrencyRate = 68.0 },
                        new { Id = 3, CurrencyCode = "USD", CurrencyName = "������", CurrencyRate = 1.0 },
                        new { Id = 4, CurrencyCode = "EUR", CurrencyName = "����", CurrencyRate = 0.9 }
                    );
                });

            modelBuilder.Entity("MVC_Core_lesson.Models.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address");

                    b.Property<string>("ContactPhone");

                    b.Property<int>("PhoneId");

                    b.Property<string>("User");

                    b.HasKey("Id");

                    b.HasIndex("PhoneId");

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("MVC_Core_lesson.Models.Phone", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("CategoryId");

                    b.Property<int>("CompanyId");

                    b.Property<DateTime>("Date");

                    b.Property<bool>("InStock");

                    b.Property<int>("MoneyId");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<double>("Price");

                    b.HasKey("Id");

                    b.HasIndex("CategoryId");

                    b.HasIndex("CompanyId");

                    b.HasIndex("MoneyId");

                    b.ToTable("Phones");

                    b.HasData(
                        new { Id = 1, CategoryId = 1, CompanyId = 1, Date = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), InStock = false, MoneyId = 3, Name = "iphone X", Price = 980.0 },
                        new { Id = 2, CategoryId = 2, CompanyId = 2, Date = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), InStock = true, MoneyId = 3, Name = "Note 9", Price = 780.0 },
                        new { Id = 3, CategoryId = 2, CompanyId = 4, Date = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), InStock = false, MoneyId = 3, Name = "Mi Max 2", Price = 250.0 },
                        new { Id = 4, CategoryId = 2, CompanyId = 5, Date = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), InStock = true, MoneyId = 3, Name = "One Plus", Price = 950.0 }
                    );
                });

            modelBuilder.Entity("MVC_Core_lesson.Models.PhoneOnStock", b =>
                {
                    b.Property<int>("PhoneId");

                    b.Property<int>("StockId");

                    b.Property<int>("Qty");

                    b.HasKey("PhoneId", "StockId");

                    b.HasIndex("StockId");

                    b.ToTable("PhoneOnStocks");
                });

            modelBuilder.Entity("MVC_Core_lesson.Models.Stock", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Stocks");

                    b.HasData(
                        new { Id = 1, Name = "Склад 1" },
                        new { Id = 2, Name = "Склад 2" },
                        new { Id = 3, Name = "Склад 3" }
                    );
                });

            modelBuilder.Entity("MVC_Core_lesson.Models.Category", b =>
                {
                    b.HasOne("MVC_Core_lesson.Models.Category", "ParentCategory")
                        .WithMany("SubCategories")
                        .HasForeignKey("ParentCategoryId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("MVC_Core_lesson.Models.Company", b =>
                {
                    b.HasOne("MVC_Core_lesson.Models.Phone")
                        .WithMany("Companies")
                        .HasForeignKey("PhoneId");
                });

            modelBuilder.Entity("MVC_Core_lesson.Models.Description", b =>
                {
                    b.HasOne("MVC_Core_lesson.Models.Phone", "Phone")
                        .WithMany("Descriptions")
                        .HasForeignKey("PhoneId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("MVC_Core_lesson.Models.Money", b =>
                {
                    b.HasOne("MVC_Core_lesson.Models.Phone")
                        .WithMany("Monies")
                        .HasForeignKey("PhoneId");
                });

            modelBuilder.Entity("MVC_Core_lesson.Models.Order", b =>
                {
                    b.HasOne("MVC_Core_lesson.Models.Phone", "Phone")
                        .WithMany("Orders")
                        .HasForeignKey("PhoneId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("MVC_Core_lesson.Models.Phone", b =>
                {
                    b.HasOne("MVC_Core_lesson.Models.Category", "Category")
                        .WithMany("Phones")
                        .HasForeignKey("CategoryId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("MVC_Core_lesson.Models.Company", "Company")
                        .WithMany("Phones")
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("MVC_Core_lesson.Models.Money", "Money")
                        .WithMany("Phones")
                        .HasForeignKey("MoneyId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("MVC_Core_lesson.Models.PhoneOnStock", b =>
                {
                    b.HasOne("MVC_Core_lesson.Models.Phone", "Phone")
                        .WithMany("Stocks")
                        .HasForeignKey("PhoneId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("MVC_Core_lesson.Models.Stock", "Stock")
                        .WithMany("Phones")
                        .HasForeignKey("StockId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
