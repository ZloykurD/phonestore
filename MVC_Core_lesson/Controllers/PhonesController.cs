﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MVC_Core_lesson.Database;
using MVC_Core_lesson.Enums;
using MVC_Core_lesson.Models;
using MVC_Core_lesson.ViewModels;

namespace MVC_Core_lesson.Controllers
{
  public class PhonesController : Controller
  {
    private readonly ApplicationContext _context;
    public int pageSize = 3;

    private IHostingEnvironment _environment;

    public PhonesController(ApplicationContext context, IHostingEnvironment environment)
    {
      _context = context;
      _environment = environment;
    }

    // GET: Phones
    public async Task<IActionResult> Index(
      string name,
      string CategoryName,
      double? priceFrom,
      double? priceTo,
      int? companyId,
      int? categoryId,
      int? moneyId,
      bool? inStock,
      SortPhones sortPhones = SortPhones.NameAsc,
      int page = 1
    )
    {
      IQueryable<Phone> phones = _context.Phones
        .Include(p => p.Category)
        .Include(p => p.Company)
        .Include(m => m.Money);
        
      if (!string.IsNullOrWhiteSpace(name))
      {
        phones = phones.Where(s => s.Name.Contains(name));
      }

      if (!string.IsNullOrWhiteSpace(CategoryName))
      {
        phones = phones.Where(s => s.Category.Name.Contains(CategoryName));
      }
      
      if (priceFrom.HasValue)
      {
        phones = phones.Where(s => s.Price >= priceFrom.Value);
      }

      if (priceTo.HasValue)
      {
        phones = phones.Where(s => s.Price <= priceTo.Value);
      }


      if (companyId.HasValue)
      {
        phones = phones.Where(s => s.CompanyId == companyId.Value);
      }

      if (categoryId.HasValue)
      {
        phones = phones.Where(s => s.CategoryId == categoryId.Value);
      }

      if (moneyId.HasValue)
      {
        phones = phones.Where(s => s.MoneyId == moneyId.Value);
      }

      ViewBag.NameSort = sortPhones == SortPhones.NameAsc ? SortPhones.NameDesc : SortPhones.NameAsc;
      ViewBag.InStockSort = sortPhones == SortPhones.InStockAsc ? SortPhones.InStockDesc : SortPhones.InStockAsc;
      ViewBag.PriceSort = sortPhones == SortPhones.PriceAsc ? SortPhones.PriceDesc : SortPhones.PriceAsc;
      ViewBag.CompanySort = sortPhones == SortPhones.CompanyAsc ? SortPhones.CompanyDesc : SortPhones.CompanyAsc;
      ViewBag.MoneySort = sortPhones == SortPhones.MoneyAsc ? SortPhones.MoneyDesc : SortPhones.MoneyAsc;
      ViewBag.CategorySort = sortPhones == SortPhones.CategoryAsc ? SortPhones.CategoryDesc : SortPhones.CategoryAsc;


      switch (sortPhones)
      {
        case SortPhones.NameAsc:
          phones = phones.OrderBy(s => s.Name);
          break;
        case SortPhones.NameDesc:
          phones = phones.OrderByDescending(s => s.Name);
          break;
        case SortPhones.CategoryNameAsc:
          phones = phones.OrderBy(s => s.Category.Name);
          break;
        case SortPhones.CategoryNameDesc:
          phones = phones.OrderByDescending(s => s.Category.Name);
          break;
        case SortPhones.PriceAsc:
          phones = phones.OrderBy(s => s.Price);
          break;
        case SortPhones.PriceDesc:
          phones = phones.OrderByDescending(s => s.Price);
          break;
        case SortPhones.CompanyAsc:
          phones = phones.OrderBy(s => s.Company.Name);
          break;
        case SortPhones.CompanyDesc:
          phones = phones.OrderByDescending(s => s.Company.Name);
          break;
        case SortPhones.MoneyAsc:
          phones = phones.OrderBy(s => s.Money.CurrencyCode);
          break;
        case SortPhones.MoneyDesc:
          phones = phones.OrderByDescending(s => s.Money.CurrencyCode);
          break;
        case SortPhones.CategoryAsc:
          phones = phones.OrderBy(s => s.Category.Name);
          break;
        case SortPhones.CategoryDesc:
          phones = phones.OrderByDescending(s => s.Category.Name);
          break;
        case SortPhones.InStockAsc:
          phones = phones.OrderBy(s => s.InStock == true);
          break;
        case SortPhones.InStockDesc:
          phones = phones.OrderByDescending(s => s.InStock != true);
          break;
      }

      PageViewModel viewModel = new PageViewModel(phones.Count(), page, pageSize);
      
      IndexPhoneViewModel model = new IndexPhoneViewModel()
      {
        Phones = phones.Skip((page - 1)
                             * (pageSize)).Take(pageSize).ToList(),
        Name = name,
        CompanyId = companyId,
        MoneyId = moneyId,
        CategoryId = categoryId,
        PriceFrom = priceFrom,
        PriceTo = priceTo,
        Companies = new SelectList(_context.Companies, "Id", "Name"),
        Moneis = new SelectList(_context.Monies, "Id", "CurrencyCode"),
        Categories = new SelectList(_context.Categories, "Id", "Name"),
        ViewModel = viewModel
      };

      return View(model);
    }

    // GET: Phones/Details/5
    public async Task<IActionResult> Details(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      ViewData["MoneyId"] = new SelectList(_context.Monies, "Id", "CurrencyCode");
      var phone = await _context.Phones
        .Include(p => p.Category)
        .Include(p => p.Company)
        .Include(m => m.Money)
        .Include(p => p.Descriptions)
        .FirstOrDefaultAsync(m => m.Id == id);


      ViewBag.PhoneList = _context.Descriptions
        .Include(d => d.Phone).Where(m => m.Id == id).ToList();


      if (phone == null)
      {
        return NotFound();
      }

      return View(phone);
    }

    // GET: Phones/Create
    public IActionResult Create()
    {
      ViewData["CategoryId"] = new SelectList(_context.Categories, "Id", "Name");
      ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Name");
      ViewData["MoneyId"] = new SelectList(_context.Monies, "Id", "CurrencyCode");
      return View();
    }

    // POST: Phones/Create
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("Id,Name,CompanyId,Price,CategoryId,MoneyId,InStock")]
      Phone phone)
    {
      if (ModelState.IsValid)
      {
        _context.Add(phone);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
      }

      ViewData["CategoryId"] = new SelectList(_context.Categories, "Id", "Name", phone.CategoryId);
      ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Name", phone.CompanyId);
      ViewData["MoneyId"] = new SelectList(_context.Monies, "Id", "CurrencyCode", phone.MoneyId);
      return View(phone);
    }

    // GET: Phones/Edit/5
    public async Task<IActionResult> Edit(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      var phone = await _context.Phones.FindAsync(id);
      if (phone == null)
      {
        return NotFound();
      }

      ViewData["CategoryId"] = new SelectList(_context.Categories, "Id", "Name", phone.CategoryId);
      ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Name", phone.CompanyId);
      ViewData["MoneyId"] = new SelectList(_context.Monies, "Id", "CurrencyCode", phone.MoneyId);
      return View(phone);
    }

    // POST: Phones/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, [Bind("Id,Name,CompanyId,Price,CategoryId,MoneyId,InStock")]
      Phone phone)
    {
      if (id != phone.Id)
      {
        return NotFound();
      }

      if (ModelState.IsValid)
      {
        try
        {
          _context.Update(phone);
          await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
          if (!PhoneExists(phone.Id))
          {
            return NotFound();
          }
          else
          {
            throw;
          }
        }

        return RedirectToAction(nameof(Index));
      }

      ViewData["CategoryId"] = new SelectList(_context.Categories, "Id", "Name", phone.CategoryId);
      ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Name", phone.CompanyId);
      ViewData["MoneyId"] = new SelectList(_context.Monies, "Id", "CurrencyCode", phone.MoneyId);
      return View(phone);
    }

    // GET: Phones/Delete/5
    public async Task<IActionResult> Delete(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      var phone = await _context.Phones
        .Include(p => p.Category).Include(m => m.Money)
        .FirstOrDefaultAsync(m => m.Id == id);
      if (phone == null)
      {
        return NotFound();
      }

      return View(phone);
    }

    // POST: Phones/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
      var phone = await _context.Phones.FindAsync(id);
      _context.Phones.Remove(phone);
      await _context.SaveChangesAsync();
      return RedirectToAction(nameof(Index));
    }

    private bool PhoneExists(int id)
    {
      return _context.Phones.Any(e => e.Id == id);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> EditCurrency(int id, [Bind("Id,Name,Company,Price,CategoryId,MoneyId")]
      Phone phone)
    {
      if (!ModelState.IsValid)
      {
        return NotFound();
      }


      double newPrice = 0;
      switch (phone.MoneyId)
      {
        case 1: //"RUB"
          if (phone.Price > 1000)
          {
            newPrice = phone.Price / 63;
          }
          else
          {
            newPrice = phone.Price * 57;
          }

          break;
        case 2: //"KGS"
          if (phone.Price > 1000)
          {
            newPrice = phone.Price / 68;
          }
          else
          {
            newPrice = phone.Price * 68;
          }

          break;
        case 3: //"USD"
          if (phone.Price > 1000)
          {
            newPrice = phone.Price / 68;
          }
          else
          {
            newPrice = phone.Price * 1;
          }

          break;
        case 4: // "EUR"
          if (phone.Price > 1000)
          {
            newPrice = phone.Price / 68;
          }
          else
          {
            newPrice = phone.Price * 0.9;
          }

          break;
      }

      phone.Price = newPrice;
      _context.Update(phone);
      _context.SaveChanges();

      ViewData["CategoryId"] = new SelectList(_context.Categories, "Id", "Name", phone.CategoryId);
      ViewData["MoneyId"] = new SelectList(_context.Monies, "Id", "CurrencyCode", phone.CategoryId);


      return RedirectToAction("Details", new {id = phone.Id});
    }


    public IActionResult Download(String id)
    {
      try
      {
        String filePath = Path.Combine(_environment.ContentRootPath, $"Files/phone_{id}.pdf");
        String fileType = "application/pdf";
        String fileName = $"phone_{id}.pdf";
        if (!System.IO.File.Exists(filePath))
        {
          throw new FileNotFoundException($"Файл не найден");
        }

        return PhysicalFile(filePath, fileType, fileName);
      }
      catch (Exception e)
      {
        ViewData["Message"] = e.Message;
        return View("DownloadError");
      }
    }


    public IActionResult OnCompanySite(String Company)
    {
      return Redirect($"http://www.{Company}.com");
    }
  }
}