﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MVC_Core_lesson.Database;
using MVC_Core_lesson.Models;
using MVC_Core_lesson.ViewModels;

namespace MVC_Core_lesson.Controllers
{
  public class HomeController : Controller
  {
    ApplicationContext db = new ApplicationContext();

    public HomeController(ApplicationContext context)
    {
      db = context;
    }

    [HttpGet]
    public async Task<IActionResult> Index(int? companyId, string name)
    {
      List<Company> companies = db.Companies.ToList();

      var phones = db.Phones.Include(p => p.Category).Include(p => p.Company).Include(m => m.Money).ToList();

      IndexViewModel ivm = new IndexViewModel();

      if (companyId.HasValue)
      {
        phones = phones.Where(p => p.Company.Id == companyId.Value).ToList();
        ivm.Company = companies.FirstOrDefault(c => c.Id == companyId.Value);
      }

      if (!string.IsNullOrWhiteSpace(name))
      {
        phones = phones.Where(p => p.Name.Contains(name, StringComparison.OrdinalIgnoreCase)).ToList();
        ivm.Name = name;
      }

      ivm.Companies = companies;
      ivm.Phones = phones;

      return View(ivm);
    }

    [HttpPost]
    public async Task<IActionResult> Index(SearchResult result)
    {
      string name = result.Name;

      double max = result.MaxPrice;
      double min = result.MinPrice;
      //bool InStock = result.isCheck;


      IndexViewModel ivm = new IndexViewModel();
      List<Company> companies = db.Companies.ToList();
      var phones = db.Phones.Include(p => p.Category).Include(p => p.Company)
        .Include(m => m.Money).ToList();

      if (result!=null)
      {
       var r = result;
      }
      else
      {
        ViewBag.Message = $"Не верно выбрана фильтрация";
        return View();
      }






      if (!string.IsNullOrWhiteSpace(name))
      {
        phones = phones.Where(p => p.Name.Contains(name, StringComparison.OrdinalIgnoreCase)).ToList();
        ivm.Name = name;
        
        if (min == 0 && max == 0 && !result.isCheck)
        {
          ivm.Companies = companies;
          ivm.Phones = phones;

          return View(ivm);
        }

        if (min != null && max != null && result.isCheck)
        {
          if (min < max)
          {
            phones.Where(p => p.InStock == result.isCheck && p.Price >= min && p.Price >= max);
          }
          else
          {
            ViewBag.Message = $"Не верно выбрана фильтрация {min} больше {max}";
            return View();
          }
        }
        else if (min != null && max != null)
        {
          if (min < max)
          {
            phones.Where(p => p.Price >= min && p.Price >= max);
          }
          else
          {
            ViewBag.Message = $"Не верно выбрана фильтрация {min} больше {max}";
            return View();
          }
        }
      }
      else
      {
        if (min != null && max != null && result.isCheck)
        {
          if (min < max)
          {
            phones.Where(p => p.InStock == result.isCheck && p.Price >= min && p.Price >= max);
          }
          else
          {
            ViewBag.Message = $"Не верно выбрана фильтрация {min} больше {max}";
            return View();
          }
        }
        else if (min != null && max != null)
        {
          if (min < max)
          {
            phones.Where(p => p.Price >= min && p.Price >= max);
          }
          else
          {
            ViewBag.Message = $"Не верно выбрана фильтрация {min} больше {max}";
            return View();
          }
        }

      }



      ivm.Companies = companies;
      ivm.Phones = phones;

      return View(ivm);
    }


    public IActionResult About()
    {
      return View();
    }

    public IActionResult Contact()
    {
      return View();
    }

    public IActionResult Privacy()
    {
      return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
      return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
    }
  }
}