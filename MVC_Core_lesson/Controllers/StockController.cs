﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders.Physical;
using Microsoft.Extensions.Hosting;
using MVC_Core_lesson.Database;
using MVC_Core_lesson.Models;

namespace MVC_Core_lesson.Controllers
{
  public class StockController : Controller
  {
    ApplicationContext db = new ApplicationContext();
    private IHostingEnvironment _environment;

    public StockController(ApplicationContext context, IHostingEnvironment environment)
    {
      db = context;
      _environment = environment;
    }

    // GET: Stock
    public async Task<IActionResult> Index()
    {
      return View(await db.Stocks.OrderBy(s => s.Name).ToListAsync());
    }

    // GET: Stock/Details/5
    public ActionResult Details(int id)
    {
      Stock stock = db.Stocks.Find(id);
      if (stock != null)
      {
        return View(stock);
      }

      return NotFound($"Нет склада с id {id}");
    }

    public IActionResult Download(String id)
    {
      String filePath = Path.Combine(_environment.ContentRootPath, $"Files/stock_{id}.pdf");
      String fileType = "application/pdf";
      String fileName = $"stock_{id}.pdf";
      return PhysicalFile(filePath, fileType, fileName);
    }

    // GET: Stock/Create
    public ActionResult Create()
    {
      return View();
    }

    // POST: Stock/Create
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create(Stock stock)
    {
      try
      {
        if (ModelState.IsValid)
        {
          // TODO: Add insert logic here


          return RedirectToAction(nameof(Index));
        }
        else
        {
          return View();
        }
      }
      catch
      {
        return View();
      }
    }

    // GET: Stock/Edit/5
    public ActionResult Edit(int id)
    {
      Stock stock = db.Stocks.Find(id);
      return View(stock);
    }

    // POST: Stock/Edit/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit(Stock stock)
    {
      try
      {
        // TODO: Add update logic here
        db.Stocks.Update(stock);
        db.SaveChanges();
        return RedirectToAction(nameof(Index));
      }
      catch
      {
        return View();
      }
    }

    // GET: Stock/Delete/5
    public ActionResult Delete(int id)
    {
      return View();
    }

    // POST: Stock/Delete/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Delete(int id, IFormCollection collection)
    {
      try
      {
        // TODO: Add delete logic here

        return RedirectToAction(nameof(Index));
      }
      catch
      {
        return View();
      }
    }
  }
}