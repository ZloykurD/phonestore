﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MVC_Core_lesson.Database;
using MVC_Core_lesson.Models;

namespace MVC_Core_lesson.Controllers
{
  public class ValidationController : Controller
  {
    private readonly ApplicationContext db;

    public ValidationController(ApplicationContext context)
    {
      db = context;
    }


    // GET: Validation
    public ActionResult Index()
    {
      return View();
    }

    [AcceptVerbs("Get", "Post")]
    public ActionResult ValidateName(String name)
    {
      String[] arr = new string[] {"тест", "fuck"};

      if (arr.Any(b => name.Contains(b, StringComparison.CurrentCultureIgnoreCase)))
      {
        return Json(false);
      }

      return Json(true);
    }

    [AcceptVerbs("Get", "Post")]
    public ActionResult ValidateCompanyName(String name)
    {
      String[] arr = new string[] {"тест", "fuck"};
      Company company = db.Companies.FirstOrDefault(n => n.Name.Equals(name));
      if (company != null)
      {
        if (company.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase))
        {
          return Json(false);
        }
      }


      return Json(true);
    }

    // GET: Validation/Details/5
    public ActionResult Details(int id)
    {
      return View();
    }

    // GET: Validation/Create
    public ActionResult Create()
    {
      return View();
    }

    // POST: Validation/Create
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create(IFormCollection collection)
    {
      try
      {
        // TODO: Add insert logic here

        return RedirectToAction(nameof(Index));
      }
      catch
      {
        return View();
      }
    }

    // GET: Validation/Edit/5
    public ActionResult Edit(int id)
    {
      return View();
    }

    // POST: Validation/Edit/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit(int id, IFormCollection collection)
    {
      try
      {
        // TODO: Add update logic here

        return RedirectToAction(nameof(Index));
      }
      catch
      {
        return View();
      }
    }

    // GET: Validation/Delete/5
    public ActionResult Delete(int id)
    {
      return View();
    }

    // POST: Validation/Delete/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Delete(int id, IFormCollection collection)
    {
      try
      {
        // TODO: Add delete logic here

        return RedirectToAction(nameof(Index));
      }
      catch
      {
        return View();
      }
    }
  }
}