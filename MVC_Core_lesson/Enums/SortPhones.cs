﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_Core_lesson.Enums
{
  public enum SortPhones
  {
    NameAsc,
    NameDesc,
    CategoryNameAsc,
    CategoryNameDesc,
    CategoryAsc,
    CategoryDesc,
    PriceAsc,
    PriceDesc,
    CompanyAsc,
    CompanyDesc,
    MoneyAsc,
    MoneyDesc,
    InStockAsc,
    InStockDesc,

  }
}
