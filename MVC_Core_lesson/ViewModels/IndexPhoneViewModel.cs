﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using MVC_Core_lesson.Models;

namespace MVC_Core_lesson.ViewModels
{
  public class IndexPhoneViewModel : Phone
  {
    public IEnumerable<Phone> Phones { get; set; }
 
    [Display(Name = "от")]
    public double? PriceFrom { get; set; }
    [Display(Name = "до")]
    public double? PriceTo { get; set; }

    public new int? CompanyId { get; set; }
    public new int? MoneyId { get; set; }
    public new int? CategoryId { get; set; }
    
    public PageViewModel ViewModel { get; set; }

    public SelectList Companies { get; set; }
    public SelectList Moneis { get; set; }
    public SelectList Categories { get; set; }

  }
}
