﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using MVC_Core_lesson.Models;

namespace MVC_Core_lesson.ViewModels
{
  public class IndexViewModel
  {
    public IEnumerable<Phone> Phones { get; set; }
    public IEnumerable<Company> Companies { get; set; }
    [Display(Name = "Производитель")]
    public Company Company { get; set; }
    [Display(Name = "Модель")]
    public string Name { get; set; }
    [Display(Name = "Цена")]
    public string Price { get; set; }
    [Display(Name = "Валюта")]
    public string Currency { get; set; }
    [Display(Name = "Категория")]
    public string Category { get; set; }
  }
}
